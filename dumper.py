
# coding: utf8

import sys

def main():

    if len(sys.argv) < 2:
        usage()
        exit(0)

    rangeArg = ""
    rangeMin = 0
    rangeMax = 0
    format = ""
    inputPathFile = ""
    outPathFile = ""
    data = ""

#Parse parameters
    for i in range(0,len(sys.argv)):
        cur = sys.argv[i]

        #get optional range
        if len(sys.argv) > i+1 and (cur == "-r" or cur == "--range"):
            rangeArg = sys.argv[i+1]

        #get optional format
        if len(sys.argv) > i+1 and (cur == "-f" or cur == "--format"):
            format = sys.argv[i+1]

        # get optional format
        if len(sys.argv) > i + 1 and (cur == "-o" or cur == "--output"):
            outPathFile = sys.argv[i + 1]

        #print usage and exit
        if cur == "-h" or cur == "--help":
            usage()

        #get input file
        if len(sys.argv) > i + 1 and (cur == "-i" or cur == "--input"):
            inputPathFile = sys.argv[i+1]

    if inputPathFile:
        data = open(inputPathFile, "rb").read()
    else:
        print("[ERROR]: Missing input path")
        exit(0)

    if rangeArg:
        rangeMin, rangeMax = getRange(rangeArg, len(data))
        if rangeMin >= rangeMax:
            print("[ERROR] null or negative range")
            exit(0)
        if rangeMin == rangeMax == 0 or rangeMin < 0 or rangeMax > len(data):
            print("[ERROR] Range not valid, format is 'beginOffet:endOffset'")
            exit(0)
    else:
        rangeMin = 0
        rangeMax = len(data)

    if format == "c" or format == "C":
        format = "c"
    elif format == "python" or format == "p":
        format = "p"
    elif format == "raw" or format == "r":
        format = "r"
    else:
        print("[ERROR] Invalid format")
        exit(0)

    generate(data[rangeMin:rangeMax+1],format,outPathFile)

def usage():
    print("Usage: "+sys.argv[0]+ " <-i | --input> <inputFile> [Options]")
    print("Options :")
    print("  - [-h | --help]: Print this help")
    print("  - [-o | --output] <outputFile>: Path to the output file")
    print("  - [-r | --range <Min:Max>]: Choose range to dump, separated by ':'")
    print("        -r 0x100:0x300   -> Dump 0x200 bytes from offset 0x100")
    print("        -r 100:          -> Dump from offset 100 to end of file")
    print("        -r :200          -> Dump first 200 bytes, it's same that '-r 0:200'")
    print("  - [-f | --format]: Choose format output in order to include the dump in a code")
    print("        -f c | -f C      -> Generate a unsigned char buffer containing the dump")
    print("        -f p | -f python -> Generate a python array containing the dump")
    print("        -f r | -f raw    -> Dump selected bloc without handling")
    exit(0)

def integer(number):
    if not number:
        return 0

    if len(number) >= 3 and (number[:2] == "0x" or number[:2] == "0X"):
        return int(number,0x10)
    return int(number)

def getRange(range,max):
    rangeMin = 0
    rangeMax = 0
    offSeparator = range.find(":")
    if offSeparator == -1:
        return 0,0
    elif offSeparator == 0:
        rangeMin = 0
        rangeMax = range[1:]
    elif offSeparator == (len(range) - 1):
        rangeMin = range[0:len(range) - 1]
        rangeMax = max
    else:
        rangeMin = range[:offSeparator]
        rangeMax = range[offSeparator+1:]

    return integer(str(rangeMin)),integer(str(rangeMax))

def generate(data,format,pathOutput):
    output = ""
    firstLoop = True
    if format == "c":
        output += "int bufferSize = "+str(len(data))+ ";\nunsigned char buffer["+str(len(data))+"] = {\n"
    if format == "p":
        output += "#Buffer size: "+str(len(data))+" bytes\nbuffer =  \""
    if format == "r":
        if pathOutput:
            f = open(pathOutput,"wb")
            f.write(data)
            f.close()
        else:
            for i in range(0, len(data)):
                print(chr(data[i]),end="")
            print();
        return

    for i in range(0,len(data)):
        #get hexa value
        if data[i] < 0x10:
            hexa = "0"+hex(data[i])[2:]
        else:
            hexa = hex(data[i])[2:]

        #end of line
        if i % 0x10 == 0 and not firstLoop:
            if format == "p":
                output += "\"\nbuffer += \"\\x" + hexa
            else:
                output += ",\n0x"+hexa

        #append to current line
        else:
            if format == "p":
                output += "\\x" + hexa
            else:
                if firstLoop:
                    output += "0x"+hexa
                else:
                    output += ",0x"+hexa
        firstLoop = False		
				
    if format == "c":
        output += "\n};"
    if format == "p":
        output += "\"\n"
    if pathOutput:
        f = open(pathOutput,"w")
        f.write(output)
        f.close()
    else:
        print(output)

if __name__ == '__main__':
    main()